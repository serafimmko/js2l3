// TODO: читер)))
const app = new Vue({
  el: "#app",
  data: {
    name: "name",
    send: "send",
    phone: "+7(000)000-0000",
    email: "1@1.1",
    text: "text",
    dinamicClassName: "validate",
    dinamicClassPhone: "validate",
    dinamicClassEmail: "validate",
    dinamicClassText: "validate"
  },
  methods: {
    validateAndSend: function() {
      //TODO: вот это if-else нельзя ли разбить на отдельные функции?
      // было б более читабельно
      // upd. да, можно) синтаксис реактовский, но anyway
      // upd2. чота мне оба варианта не особо нравятся.
      let flag = true;
      const checkName = () => this.name.search(/\s+|\d+/) !== -1;
      checkName()
        ? ((this.dinamicClassName = "invalidate"),
          alert(`fcked error! in the name: ${this.name}`),
          (flag = false))
        : (this.dinamicClassName = "validate");

      // if (this.name.search(/\s+|\d+/) !== -1) {
      //   this.dinamicClassName = "invalidate";
      //   alert(`fcked error! in the name: ${this.name}`);
      //   flag = false;
      // } else {
      //   this.dinamicClassName = "validate";
      // }
      // TODO: _ пропускает, как минимум
      if (this.email.search(/[\w+]?(.)?(\w+?)@\w+?.\w+?/)) {
        this.dinamicClassEmail = "invalidate";
        alert(`fcked error! in the e-mail: ${this.email}`);
        flag = false;
      } else {
        this.dinamicClassEmail = "validate";
      }
      //TODO: уточняй паттерн, ибо пропускает пробелы в конце. trim() поможет
      if (this.phone.search(/\+\d{1}\(\d{3}\)\d{3}-\d{4}/)) {
        this.dinamicClassPhone = "invalidate";
        alert(`fcked error! in the phone: ${this.phone}`);
        flag = false;
      } else {
        this.dinamicClassPhone = "validate";
      }
      if (flag) alert("all good");
    }
  }
});
